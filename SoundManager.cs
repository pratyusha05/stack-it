﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : MonoBehaviour
{

    public static AudioClip dolowSound, reSound, miSound, faSound, soSound, laSound, tiSound, dohighSound, wrongSound;
    static AudioSource audioSrc;

    void Start()
    {
        dolowSound = Resources.Load<AudioClip>("dolow");
        reSound = Resources.Load<AudioClip>("re");
        miSound = Resources.Load<AudioClip>("mi");
        faSound = Resources.Load<AudioClip>("fa");
        soSound = Resources.Load<AudioClip>("so");
        laSound = Resources.Load<AudioClip>("la");
        tiSound = Resources.Load<AudioClip>("ti");
        dohighSound = Resources.Load<AudioClip>("dohigh");
        wrongSound = Resources.Load<AudioClip>("wrong2again3");


        audioSrc = GetComponent<AudioSource>();
    }

    public static void PlaySound(int combo)
    {
      
        switch (combo)
        {
            case 1:
                audioSrc.PlayOneShot(dolowSound);
                break;
            case 2:
                audioSrc.PlayOneShot(reSound);
                break;
            case 3:
                audioSrc.PlayOneShot(miSound);
                break;
            case 4:
                audioSrc.PlayOneShot(faSound);
                break;
            case 5:
                audioSrc.PlayOneShot(soSound);
                break;
            case 6:
                audioSrc.PlayOneShot(laSound);
                break;
            case 7:
                audioSrc.PlayOneShot(tiSound);
                break;
            case 8:
                audioSrc.PlayOneShot(dohighSound);
                break;
            default:
                audioSrc.PlayOneShot(wrongSound);
                break;
                
        
        }
    }
}
