﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;
using System;

public class MainMenu : MonoBehaviour
{
    public Text scoreText;


     private void Start()
     {
         scoreText.text = PlayerPrefs.GetInt("score").ToString();
     }


    private void SetText(TextMesh textMesh)
    {
        throw new NotImplementedException();
    }

    public void ToGame(string scene)
    {
        SceneManager.LoadScene(scene);

    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
